// Importar el módulo body-parser y el framework Express
const bodyParser = require("body-parser");
const express = require("express");

// Crear una instancia de Router
const router = express.Router();

// Importar Axios

const AlumnosDb = require('../models/alumnos.js');

alumno = {}

/*
// Ruta principal que muestra una página con los datos del array "datos"
router.get('/', async (req, res) => {
  try {
    const alumnos = await AlumnosDb.mostrarTodos();
    res.render('index.html', { alumnos: alumnos });
  } catch (error) {
    console.error(error);
    res.status(500).send('Error al cargar la lista de alumnos.');
  }
});
*/

router.get("/mostrarTodos", async(req, res) => {
  AlumnosDb.mostrarTodos()
  .then(alumnos => {
      res.json(alumnos);
  })
  .catch(error => {
      console.error(error);
      res.status(500).send(error);
  });
});


//Ejemplo para insertar q nos dio el profe
router.post("/insertar", async(req, res) => {
  // Se obtienen los valores enviados en el formulario
  const alumno = {
    matricula: req.body.matricula,
    nombre: req.body.nombre,
    domicilio: req.body.domicilio,
    sexo: req.body.sexo,
    especialidad: req.body.especialidad
  };

  resultado = await AlumnosDb.insertar(alumno);
  res.json(resultado);
});

//Este es el q hizo el profe
router.post("/eliminar", async(req, res) => {
  // Se obtienen los valores enviados en el formulario
  var matricula = req.body.matricula;

  resultado = await AlumnosDb.borrar(matricula);
  res.json(resultado);
});

//Este es el q hizo el profe
router.post("/buscar", async(req, res) => {
  // Se obtienen los valores enviados en el formulario
  var matricula = req.body.matricula;

  resultado = await AlumnosDb.buscarXMatricula(matricula);
  res.json(resultado);
});

router.post("/actualizar", async (req, res) => {
  const matricula = req.body.matricula;
  const alumno = {
    nombre: req.body.nombre,
    domicilio: req.body.domicilio,
    sexo: req.body.sexo,
    especialidad: req.body.especialidad
  }
  
  // Se llama al método 'actualizar' de la base de datos y se devuelve una respuesta
  resultado = await AlumnosDb.actualizar(matricula, alumno);
  res.json(resultado);
});



// Exportar el módulo Router para que pueda ser utilizado en otros archivos
module.exports = router;